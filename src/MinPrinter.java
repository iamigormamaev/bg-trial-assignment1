

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class MinPrinter implements Runnable {

    @Override
    public void run() {
        Storage storage = Storage.getInstance();

        while (true) {
            try {
                TimeUnit.SECONDS.sleep(5);
                System.out.println(storage.getMinFromList());
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            } catch (NoSuchElementException e){

            }
        }
    }
}
