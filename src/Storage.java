

import java.util.TreeSet;

public class Storage {
    private static volatile Storage instance;

    private Storage() {
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
            return instance;
        } else return instance;
    }


    private final TreeSet<Integer> intSet = new TreeSet<>();

    public void putInList(int i) {
        synchronized (intSet) {
            intSet.add(i);
        }
    }

    public int getMinFromList() {
        synchronized (intSet) {
            int min = intSet.first();
            intSet.remove(min);
            return min;

        }
    }
}
