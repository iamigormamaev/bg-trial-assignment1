

import java.util.HashMap;
import java.util.Map;

public class NumberUtils {
    private Map<String, Integer> wordsToInteger = new HashMap<>();

    public NumberUtils() {
        wordsToInteger.put("one", 1);
        wordsToInteger.put("two", 2);
        wordsToInteger.put("three", 3);
        wordsToInteger.put("four", 4);
        wordsToInteger.put("five", 5);
        wordsToInteger.put("six", 6);
        wordsToInteger.put("seven", 7);
        wordsToInteger.put("eight", 8);
        wordsToInteger.put("nine", 9);
        wordsToInteger.put("ten", 10);
        wordsToInteger.put("eleven", 11);
        wordsToInteger.put("twelve", 12);
        wordsToInteger.put("thirteen", 13);
        wordsToInteger.put("fourteen", 14);
        wordsToInteger.put("fifteen", 15);
        wordsToInteger.put("sixteen", 16);
        wordsToInteger.put("seventeen", 17);
        wordsToInteger.put("eighteen", 18);
        wordsToInteger.put("nineteen", 19);
        wordsToInteger.put("twenty", 20);
        wordsToInteger.put("thirty", 30);
        wordsToInteger.put("forty", 40);
        wordsToInteger.put("fifty", 50);
        wordsToInteger.put("sixty", 60);
        wordsToInteger.put("seventy", 70);
        wordsToInteger.put("eighty", 80);
        wordsToInteger.put("ninety", 90);
        wordsToInteger.put("hundred", 100);
        wordsToInteger.put("thousand", 1000);
    }

    public int wordToInteger(String numeral) {
        numeral = numeral.trim();
        String args[] = numeral.split("\\s");
        int result = 0;
        for (int i = args.length - 1; i >= 0; i--) {
            try {
                int tempNum = wordsToInteger.get(args[i].toLowerCase());
                if (tempNum == 100 || tempNum == 1000) {
                    i--;
                    tempNum *= wordsToInteger.get(args[i]);
                }
                result += tempNum;
            } catch (NullPointerException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return result;
    }
}