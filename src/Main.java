

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        NumberUtils numberUtils = new NumberUtils();
        Storage storage = Storage.getInstance();
        String numeral;
        new Thread(new MinPrinter()).start();

        while (true) {
            try {
                numeral = br.readLine();
                storage.putInList(numberUtils.wordToInteger(numeral));
            } catch (IOException | IllegalArgumentException e) {
                System.out.println("Неверный ввод! Попробуйте еще раз.");
            }
        }
    }
}
